# Changelog

## Unreleased

### Fixed

- #15 Nécessite le plugin YAML, car utilise l'API PHP de Saisies.

## [4.1.1] - 2022-05-18

### Fixed

- #13 #14 Éviter une erreur fatale lors de l'appel côté public d'un formulaire d'édition d'un objet ayant des champs extras

## [4.1.0] - 2022-05-11

### Added

- Ajout d’un Readme

### Changed

- Compatible avec SPIP 4.1 et PHP 8.1
